package main

import "github.com/willbitbucket/go-api-boilerplate/app"

func main() {
	app := &app.App{}
	app.Start()
}
