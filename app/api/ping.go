package api

import (
	"net/http"
	"fmt"
	"github.com/gorilla/mux"

)

func PingHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	fmt.Fprint(w, fmt.Sprintf("{ \"vars\": \"%v\" }",vars))
}