package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/willbitbucket/go-api-boilerplate/app/store/memory"
	"encoding/json"
)
//use any other store implementation init here, no need change other lines inside the handlers
var store = memory.NewStore()


func GetRaceById(w http.ResponseWriter, r *http.Request) {

	result :=<- store.Race().Get(mux.Vars(r)["id"])
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	buff, _ := json.MarshalIndent(result.Data, "", "\t")
	w.Write(buff)
}


func GetRaces(w http.ResponseWriter, r *http.Request) {

	result :=<- store.Race().GetAll()
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	buff, _ := json.MarshalIndent(result.Data, "", "\t")
	w.Write(buff)
}