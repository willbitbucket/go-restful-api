package router

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/gorilla/mux"
	api "github.com/willbitbucket/go-api-boilerplate/app/api"
)

// Router is the custom router of our application
// it "extends" *httprouter.Router
type Router struct {
	*httprouter.Router
}

// Route is the structure of our endpoints
type Route struct {
	Method   string
	Endpoint string
	Handler  func(w http.ResponseWriter, r *http.Request, p httprouter.Params)
}




func GetRouterV2() *mux.Router {

	r := mux.NewRouter()

	r.PathPrefix("/ping").HandlerFunc(api.PingHandler)

	r.HandleFunc("/race", api.GetRaces).Methods("GET")
	r.HandleFunc("/race/{id}", api.GetRaceById).Methods("GET")

	return r
}
