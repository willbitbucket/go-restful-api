package memory

import (
	"github.com/willbitbucket/go-api-boilerplate/app/model"
	"time"
	"math/rand"
	"strconv"
	"log"
)

const(
	Thoroughbred = "Thoroughbred"
	Greyhounds = "Greyhounds"
	Harness = "Harness"

)
var Types = []string{
	Thoroughbred, Greyhounds, Harness,
}
var InMemoryData = make([]interface{},0);

func init(){

	InMemoryData = append(InMemoryData,
		model.Race{
			ID: "r1",
			CreatedOn: (time.Now()),
			Type: Types[rand.Intn(3)],
			CloseDateTime: (time.Now().Add(time.Duration(rand.Intn(160))* time.Second)),
			Competitors: []model.Competitor{
				model.Competitor{
					ID: "c"+strconv.Itoa(rand.Intn(100)),
					Position: rand.Intn(100),
				},
				model.Competitor{
					ID: "c"+strconv.Itoa(rand.Intn(100)),
					Position: rand.Intn(100),
				},
				model.Competitor{
					ID: "c"+strconv.Itoa(rand.Intn(100)),
					Position: rand.Intn(100),
				},
				model.Competitor{
					ID: "c"+strconv.Itoa(rand.Intn(100)),
					Position: rand.Intn(100),
				},
			},
		},
	)


	ticker := time.NewTicker(10 * time.Second)
	quit := make(chan struct{})
	go func() {
		for i := 2; ; i++ {
			select {
			case <- ticker.C:
				log.Println(len(InMemoryData))
				InMemoryData = append(InMemoryData,
					model.Race{
						ID: "r"+strconv.Itoa(i),
						CreatedOn: (time.Now()),
						Type: Types[rand.Intn(3)],
						CloseDateTime: (InMemoryData[len(InMemoryData)-1].(model.Race).CloseDateTime.Add(time.Duration(rand.Intn(120))* time.Second)),
						Competitors: []model.Competitor{
							model.Competitor{
								ID: "c"+strconv.Itoa(rand.Intn(100)),
								Position: rand.Intn(100),
							},
							model.Competitor{
								ID: "c"+strconv.Itoa(rand.Intn(100)),
								Position: rand.Intn(100),
							},
							model.Competitor{
								ID: "c"+strconv.Itoa(rand.Intn(100)),
								Position: rand.Intn(100),
							},
							model.Competitor{
								ID: "c"+strconv.Itoa(rand.Intn(100)),
								Position: rand.Intn(100),
							},
						},
					})
			case <- quit:
				ticker.Stop()
				return
			}
		}
	}()
}

