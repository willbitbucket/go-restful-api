package memory

import (
	"github.com/willbitbucket/go-api-boilerplate/app/store"
	"github.com/willbitbucket/go-api-boilerplate/app/util"
	"github.com/willbitbucket/go-api-boilerplate/app/model"
	"time"
)

type RaceStore struct{
	*Store
}

func NewRaceStore(store *Store) store.IRace {
	rs := &RaceStore{store}
	return rs
}

func (rs RaceStore) Get(id string) store.Channel {
	channel := make(store.Channel)

	go func() {
		result := store.Result{}
		races := util.Filter(InMemoryData, func(e interface{}) bool{
			return e.(model.Race).ID == id
		})
		if(len(races)) >0 {
			result.Data = races[0]
		}else {
			result.Data = nil
		};

		channel <- result
		close(channel)
	}()

	return channel
}


func (rs RaceStore) GetAll() store.Channel {
	channel := make(store.Channel)

	go func() {
		result := store.Result{}
		result.Data = util.FilterAndLimit(InMemoryData, func(e interface{}) bool{
			return e.(model.Race).CloseDateTime.After(time.Now())
		}, 5)
		channel <- result
		close(channel)
	}()

	return channel
}

