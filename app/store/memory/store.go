package memory

import (
	"github.com/willbitbucket/go-api-boilerplate/app/store"
)

type Store struct {
	race              store.IRace
}

func NewStore() store.IStore {
	store := &Store{}
	store.race = NewRaceStore(store)
	return store
}


func (s Store) Race() store.IRace {
	return s.race
}

func (s Store) Close()  {

}