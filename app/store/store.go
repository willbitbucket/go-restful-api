package store

type IStore interface {
	Race() IRace
	Close()
}

type Result struct {
	Data interface{}
	Err  error
}

type Channel chan Result

type IRace interface{
	Get(id string) Channel
	GetAll() Channel
}