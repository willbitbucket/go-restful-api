package app

import (
	"fmt"
	"log"
	"net/http"

	"github.com/willbitbucket/go-api-boilerplate/app/router"
	"github.com/willbitbucket/go-api-boilerplate/config"
	mgo "gopkg.in/mgo.v2"
	"github.com/gorilla/mux"
	"github.com/willbitbucket/go-api-boilerplate/app/store"
	"github.com/rs/cors"
)

// App contains our config and our router
type App struct {
	Router *router.Router
	Config *config.Config
	DB     *mgo.Database
	RouterV2 *mux.Router
}

// ConnectToDB set the database of our app
func (app *App) ConnectToDB() {
	dbAddr := "mongodb://" + app.Config.Database.Host + ":" + app.Config.Database.Port + "/" + app.Config.Database.Name

	session, err := mgo.Dial(dbAddr)
	if err != nil {
		panic(err)
	}

	app.DB = session.DB(app.Config.Database.Name)
}

var Store *store.IStore

// Init the app
func (app *App) Init() {
	app.RouterV2 = router.GetRouterV2()
	app.Config = config.LoadConfig()

	//app.ConnectToDB()
}

// Start our application
func (app *App) Start() {
	app.Init()

	addr := app.Config.Host + ":" + app.Config.Port
	fmt.Println("API available at " + addr)



	log.Fatal(http.ListenAndServe(addr,
		cors.Default().Handler(app.RouterV2)))
}
