package util

import (
	"testing"

)

func  TestFilter(t *testing.T)  {

	if(!sliceEqual(Filter([]interface{}{1,2,3}, func(s interface{}) bool{return s.(int)<2}), []interface{}{1})){
		t.Error("Filter failed")
	}
}

func  TestFilterAndLimit(t *testing.T)  {

	if(!sliceEqual(FilterAndLimit([]interface{}{1,2,3,4,5,6,7}, func(s interface{}) bool{return s.(int)>5},1 ), []interface{}{6, 7})){
		t.Error("Filter failed")
	}
}

func sliceEqual(a []interface{},b []interface{}) bool{
	for i, v := range a { if v != b[i] { return false } }
	return true
}