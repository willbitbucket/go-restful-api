package util

func  Filter(ss []interface{}, test func(interface{}) bool) (ret []interface{}) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
		}
	}
	return ret
}


func  FilterAndLimit(ss []interface{}, test func(interface{}) bool, limit int) (ret []interface{}) {
	for _, s := range ss {
		if test(s) {
			ret = append(ret, s)
			if (len(ret) >= limit) {
				return ret
			}
		}
	}
	return ret
}

//github.com/bradfitz/slice
//func  Sort(ss []interface{}, less func(i, j int) bool){
//	return slice.Sort(ss[:], less)
//}