package model

import (
	"time"
	"encoding/json"
)

type Race struct{
	ID            string       `json:"id"`
	CreatedOn     time.Time    `json:"created_on"`
	Type          string       `json:"type"`
	CloseDateTime time.Time    `json:"closeDateTime"`
	Competitors   []Competitor `json:"competitors,omitempty"`
}

type Competitor struct{
	ID                      string       `json:"id"`
	Position                int          `json:"position"`
}

type Meeting struct{
	ID                      string       `json:"id"`
	Races                 	[]Race       `json:"position"`
	Type                    string       `json:"type"`
}

func (d *Race) MarshalJSON() ([]byte, error) {
	type Alias Race
	return json.Marshal(&struct {
		*Alias
		CloseDateTime string `json:"closeDateTime"`
		CreatedOn	string `json:"created_on"`
	}{
		Alias: (*Alias)(d),
		CloseDateTime: d.CloseDateTime.Format("2017-12-04T14:57:45.528Z"),
		CreatedOn: d.CreatedOn.Format("2017-12-04T14:57:45.528Z"),
	})
}