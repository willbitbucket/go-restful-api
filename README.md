

# go-api-boilerplate

A starter for a Golang REST api.

## Instructions

This project uses [Glide](https://github.com/Masterminds/glide) for dependencies management, so make sure you run ```glide install``` before building the app.


## what's in here
1. List top 5 races available: http://localhost:8000/race
2. get the race detail by id: http://localhost:8000/race/{id}
3. code structure follows router -> api -> store -> model 4 layer structure, where store here implement im-memory store, which will be mutated internall, see ./app/store/memroy/mock_data.go

## how to run
```
glide insall
go run ./main.go
```

## todo
1. recovery from fatal handler
2. update API
3. more tests possibly including integration test too
